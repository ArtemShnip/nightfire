﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class Controller : MonoBehaviour
{
    [SerializeField] private float speedMove = 2f;
    [SerializeField] private Button runButton;
    private bool onRun = false;
    [SerializeField] private float timeRun;
    [SerializeField] private float stopTimeRun;
    [SerializeField] private float speedRun;
    [SerializeField] private AudioSource audio;
    private float gravityForce;
    private Vector3 moveVector;
    private CharacterController characterController;
    private MobileController mobileController;
    private Coroutine coroutine;
    

    private void Start()
    {
        characterController = GetComponent<CharacterController>();
        mobileController = GameObject.FindGameObjectWithTag("Joystick").GetComponent<MobileController>();
        runButton.onClick.AddListener(Run);
    }

    private void Update()
    {
        CharacterMove();
        GamingGravity();
    }

    private void CharacterMove()
    {
        moveVector = Vector3.zero;
        moveVector.x = mobileController.Horizontal() * speedMove;
        moveVector.z = mobileController.Vertical() * speedMove;
        if (!audio.isPlaying && moveVector != Vector3.zero)
        {
            audio.Play();
            print("play");
        }
        if (Vector3.Angle(Vector3.forward,moveVector) > 1f || Vector3.Angle(Vector3.forward,moveVector) == 0)
        {
            Vector3 dir = Vector3.RotateTowards(transform.forward, moveVector, speedMove, 0.0f);
            transform.rotation = Quaternion.LookRotation(dir);
        }
        
        moveVector.y = gravityForce;
        characterController.Move(moveVector * Time.deltaTime);

        if (mobileController.Horizontal()==0 && mobileController.Vertical()==0 && audio.isPlaying)
        {
            print("stop play");
            audio.Stop();
        }
    }

    private void GamingGravity()
    {
        if (!characterController.isGrounded)
        {
            gravityForce -= 20f * Time.deltaTime;
        }
        else
        {
            gravityForce -= 1f;
        }
    }

    private void Run()
    {
        if (!onRun)
        {
            coroutine = StartCoroutine(StartRun());
        }
    }

    private IEnumerator StartRun()
    {
        audio.pitch += 0.5f;
        onRun = true;
        runButton.image.color = new Color(1f,1f,1f,0.5f);
        speedMove += speedRun;
        yield return new WaitForSeconds(timeRun);
        speedMove -= speedRun;
        audio.pitch -= 0.5f;
        
        yield return new WaitForSeconds(stopTimeRun);
        
        runButton.image.color = new Color(1f,1f,1f,1f);
        onRun = false;
    }
}
