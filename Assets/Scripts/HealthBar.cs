﻿using UnityEngine;
using UnityEngine.UI;

public class HealthBar : MonoBehaviour
{
    [SerializeField] private Image bar;
    [SerializeField] public float fill = 1f;
    [SerializeField] private Text text;
    [SerializeField] private float SpeedHP = 0.03f;
    [SerializeField] private GameObject canvas;
    public static float TimePassed; 
    
    [SerializeField] public Text TimeText;
    [SerializeField] public Text TimeTextPause;
    [SerializeField] private Text TimeTextGameover;

    private void Start()
    {
        TimePassed = 0;
    }

    void Update()
    {
        bar.fillAmount = fill;
        fill -= Time.deltaTime * SpeedHP;
        string[] tmp = text.text.Split('/');
        text.text = tmp[0] + "/ " + (int)(fill*100);

        if (fill >= 1f)
        {
            fill = 1f;
        }
        
        TimePassed += Time.deltaTime;
        int minutes = (int) TimePassed / 60;
        int seconds = (int) TimePassed % 60;
        TimeText.text = $"{minutes:00}:{seconds:00}";
        TimeTextPause.text = $"{minutes:00}:{seconds:00}";
        
        

        if (fill <= 0)
        {
            TimeTextGameover.text =$"{minutes:00}:{seconds:00}";
            canvas.GetComponent<Canvas>().OnGameOver(true);
            SaveScore.Save(TimePassed);
        }
    }
}
