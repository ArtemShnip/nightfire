﻿using UnityEngine.SceneManagement;
using UnityEngine;
using UnityEngine.UI;

public class Canvas : MonoBehaviour
{
    [SerializeField] private GameObject mainPanel;
    [SerializeField] private GameObject pausePanel;
    [SerializeField] private GameObject gameoverPanel;

    [SerializeField] private Button pauseButton;
    
    [SerializeField] private Button resumeButtonPause;
    [SerializeField] private Button restartButtonPause;
    [SerializeField] private Button exitButtonPause;
    
    [SerializeField] private Button restartButtonGamever;
    [SerializeField] private Button exitButtonGamever;

    private void Start()
    {
        Time.timeScale = 1f;
        mainPanel.SetActive(true);
        pausePanel.SetActive(false);
        gameoverPanel.SetActive(false);
        
        pauseButton.onClick.AddListener(delegate { OnPause(true); });
        
        resumeButtonPause.onClick.AddListener(delegate { OnPause(false); });
        restartButtonPause.onClick.AddListener(OnRestart);
        exitButtonPause.onClick.AddListener(OnExit);
        
        restartButtonGamever.onClick.AddListener(OnRestart);
        exitButtonGamever.onClick.AddListener(OnExit);
    }

    private void OnRestart()
    {
        SceneManager.LoadScene("GameScene");
    }
    
    public void OnPause(bool isPause)
    {
        Time.timeScale = isPause ? 0f : 1f;
        
        mainPanel.SetActive(!isPause);
        pausePanel.SetActive(isPause);
    }

    public void OnGameOver(bool isGameover)
    {
        Time.timeScale = isGameover ? 0f : 1f;
        gameoverPanel.SetActive(true);
        mainPanel.SetActive(!isGameover);
    }

    private void OnExit()
    {
        SceneManager.LoadScene("MainMenuScene");
    }
}
