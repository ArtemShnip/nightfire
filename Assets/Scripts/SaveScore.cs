﻿using System.Collections;
using System.Collections.Generic;
using System.Xml.Linq;
using UnityEngine;
using UnityEngine.UI;

public class SaveScore : MonoBehaviour
{
    public static void Save(float timePassed)
    {
        if (PlayerPrefs.GetFloat("Score") < timePassed)
        {
            PlayerPrefs.SetFloat("Score",timePassed);
        }
    }

}
