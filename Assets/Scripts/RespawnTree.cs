﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class RespawnTree : MonoBehaviour
{
    [SerializeField] private GameObject[] treesPrefab ;
    [SerializeField] private float timeRepawn = 130f;
    
    private GameObject penek;
    private Coroutine coroutine;
    private bool OnStartRespawn;
    
    private void Start()
    {
        treesPrefab = new GameObject[2];
        treesPrefab[0] = Resources.Load<GameObject>("three");
        treesPrefab[1] = Resources.Load("three-elka",typeof(GameObject)) as GameObject;
        penek = gameObject;
        InvokeRepeating(nameof(CheckRespawn),1f,2f);
    }

    private void CheckRespawn()
    {
        if (penek.gameObject.transform.childCount == 2 && !OnStartRespawn)
        {
            coroutine = StartCoroutine(StartRespawn());
            coroutine = null;
        }
    }

    private IEnumerator StartRespawn()
    {
        OnStartRespawn = true;
        yield return new WaitForSeconds(timeRepawn);
        var treePrefab = treesPrefab[Random.Range(0,treesPrefab.Length)];
        var instantiateObject = Instantiate(treePrefab, penek.gameObject.transform.position,  penek.gameObject.transform.rotation, penek.gameObject.transform);
        instantiateObject.layer = 10;
        instantiateObject.GetComponent<Rigidbody>().isKinematic = true;

        OnStartRespawn = false;
    }
    
}
