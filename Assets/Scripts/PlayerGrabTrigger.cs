﻿using System;
using UnityEngine;

public class PlayerGrabTrigger : MonoBehaviour
{
    [SerializeField] private Transform grabPoint;
    [SerializeField] private GameObject grabObject;
    private Tree tree = new Tree();
    private Bush _bush = new Bush();

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            if (grabObject == null)
            {
                return;
            }
            FilterLayer();
        }
    }

    public void FilterLayer()
    {
        
        if (grabObject != null && grabObject.gameObject.layer == 10)
        {
            tree.FellTree(grabObject);
            return;
        }
        if (grabObject != null && grabObject.gameObject.layer == 16 && grabObject.transform.childCount > 0)
        {
            // if (grabObject.transform.childCount==0)
            // {
            //     return;
            // }
            _bush.GrabBush(grabObject);
            return;
        }
        if (grabObject.gameObject.layer < 10 || grabObject == null)
        {
            return;
        }
        else
        {
            Grab();
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (grabPoint.childCount == 0 && other.gameObject.layer > 9)
        {
            grabObject = other.gameObject;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (grabPoint.childCount == 0)
        {
            grabObject = null;
        }
    }

    private void Grab()
    {
        if (grabObject == null)
        {
            return;
        }
        
        var kin = grabObject.GetComponent<Rigidbody>();

        if (grabPoint.childCount == 0)
        {
            kin.isKinematic = true;
            grabObject.transform.rotation = Quaternion.Euler(0, 0, 0);

            grabObject.transform.position = grabPoint.transform.position;
            
            grabObject.transform.SetParent(grabPoint);
        }else
        {
            grabObject.transform.SetParent(null);
            kin.isKinematic = false;
        }
    }
}
