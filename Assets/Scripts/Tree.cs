﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tree : MonoBehaviour
{
    public void FellTree(GameObject grabObject)
    {
        var rig = grabObject.GetComponent<Rigidbody>();
        rig.isKinematic = false;
        grabObject.gameObject.layer = 15;
        var rotation = grabObject.transform.rotation;
        rotation.x += 0.2f;
        grabObject.transform.rotation = rotation;


    }
}
