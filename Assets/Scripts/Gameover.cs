﻿using UnityEngine;
using UnityEngine.UI;

public class Gameover : MonoBehaviour
{
    [SerializeField] private GameObject mainPanel;
    [SerializeField] private GameObject gameoverPanel;
    
    [SerializeField] private Button exitButton;
    [SerializeField] private Button restartButton;

    private void Start()
    {
        mainPanel.SetActive(true);
        gameoverPanel.SetActive(false);
        
        exitButton.onClick.AddListener(OnExitButton);
        restartButton.onClick.AddListener(delegate { OnRestart(false); });
    }

    private void OnRestart(bool isPause)
    {
        Time.timeScale = isPause ? 0f : 1f;
        
        mainPanel.SetActive(!isPause);
        gameoverPanel.SetActive(isPause);
    }

    private void OnExitButton()
    {
        print("EXIT");
        //SceneManager.LoadScene("MainMenu");
    }
}
