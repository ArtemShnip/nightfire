﻿using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;

public class Mushroom : MonoBehaviour
{
    [SerializeField] private float mushroomBlue = 0.15f;
    [SerializeField] private float mushroomRed = 0.20f;
    [SerializeField] private float mushroomGreen = 0.25f;
    
    public float SortMushroom(string name)
    {
        float health;
        
        switch (name)
        {
            case "MushroomBlue":
                health = mushroomBlue;
                break;
            case "MushroomRed":
                health = mushroomRed;
                break;
            case "MushroomGreen":
                health = mushroomGreen;
                break;
            default:
                health = 0;
                break;
        }

        return health;
    }
}
