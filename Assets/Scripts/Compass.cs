﻿using UnityEngine;
using UnityEngine.UI;

public class Compass : MonoBehaviour
{
    [SerializeField] private Transform player;
    [SerializeField] private float startApperingRange = 10;
    [SerializeField] private float endAppeaingRange = 70;

    private Image compasImage;

    private void Start()
    {
        compasImage = GetComponent<Image>();
    }

    private void Update()
    {
        var distance = player.position.magnitude;
        var transparency = Mathf.Clamp01((distance - startApperingRange) / (endAppeaingRange - startApperingRange));
        compasImage.color = new Color(1f,1f,1f,transparency);
        
        transform.localRotation=Quaternion.Euler(0,0,Vector3.SignedAngle(Vector3.back, player.position,Vector3.down));
    }
}
