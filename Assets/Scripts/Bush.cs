﻿using System.Collections;
using UnityEngine;

public class Bush : MonoBehaviour
{
    [SerializeField] private float health = 0.2f;
    [SerializeField] private float timeRespawn = 150f;
    [SerializeField] private GameObject fire;

    private GameObject bush;
    private bool onRespawn;
    private Coroutine coroutine;

    private void Start()
    {
        fire = GameObject.Find("fire");
        bush = gameObject.transform.gameObject;
        InvokeRepeating(nameof(CheckRespawn),1f,1f);
    }

    public void GrabBush(GameObject grabBush)
    {
        if (grabBush.transform.childCount==0)
        {
            return;
        }
        for (int i = 0; i < grabBush.transform.childCount; i++)
        {
            grabBush.transform.GetChild(i).gameObject.SetActive(false);
        }
    }
    
    private void CheckRespawn()
    {
        if (!bush.gameObject.transform.GetChild(0).gameObject.activeInHierarchy && !onRespawn)
        {
            fire.GetComponent<HealthBar>().fill += health;
            coroutine = StartCoroutine(StartRespawn());
            coroutine = null;
        }
    }

    private IEnumerator StartRespawn()
    {
        onRespawn = true;
        yield return new WaitForSeconds(timeRespawn);
        for (int i = 0; i < bush.transform.childCount; i++)
        {
            bush.transform.GetChild(i).gameObject.SetActive(true);
        }
        onRespawn = false;
    }
}