﻿using System.Collections;
using System.Text;
using UnityEngine;

public class Fire : MonoBehaviour
{
    private Coroutine Coroutine;
    private GameObject destroyObject;
    private int grabLayerMask;
    private Mushroom mushroom;
        
    
    private void Start()
    {
        mushroom = new Mushroom();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.name == "Player")
        {
            return;
        }
        if (other.transform.parent == null)
        {
            destroyObject = other.gameObject;
            other.isTrigger = true;
            StartCoroutine(SortLayer());
            Coroutine = null;
            StartCoroutine(StartDestroy());
            Coroutine = null;
        }
    }

    private IEnumerator StartDestroy()
    {
        yield return new WaitForSeconds(2f);
        Destroy(destroyObject);
    }
    
    private IEnumerator SortLayer()
    {
        var name = destroyObject.gameObject.name;
        name = TransformText(name);
        switch(destroyObject.gameObject.layer)
        {
            //firewood
            case 13:
                AddHealth(0.15f);
                break;
            //fellTree
            case 15:
                AddHealth(0.10f);
                break;
            case 14:
                AddHealth(mushroom.SortMushroom(name));
                
                break;
            default:
                break;
        }
        yield break;
    }
    
    private string TransformText(string input)
    {
        var builder = new StringBuilder();
        foreach (var symbol in input)
            if (symbol != '(')
                builder.Append(symbol);
            else
                break;
        return builder.ToString();
    }

    private void AddHealth(float health)
    {
        GetComponent<HealthBar>().fill += health;
    }
}
