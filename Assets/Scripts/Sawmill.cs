﻿using System.Collections;
using UnityEngine;

public class Sawmill : MonoBehaviour
{
    [SerializeField] private GameObject grabObject;
    [SerializeField] private Transform grabPoint;
    [SerializeField] private Transform endPointTree;
    [SerializeField] private Transform endPointFirewood;
    [SerializeField] private float speedMove = 2;
    [SerializeField] private GameObject firewoodPrefab;
    [SerializeField] private AudioSource audio;
    
    private bool onStart = false;

    private Coroutine coroutine;

    private void OnTriggerEnter(Collider other)
    {
        if (grabPoint.childCount == 0 && other.gameObject.layer == 15 && !onStart)
        {
            grabObject = other.gameObject;
            if (grabObject != null && grabObject.GetComponentInParent<PlayerGrabTrigger>() == null)
            {
                coroutine = StartCoroutine(StartMove());
                coroutine = null;
            }
        }
    }

    private IEnumerator StartMove()
    {
        
        while (onStart)
        {
            yield return new WaitForSeconds(1f);
        }

        onStart = true;
        Vector3 velocity = Vector3.zero;
        float scaleVelocity = 0;
        
        while (Vector3.Distance(grabObject.transform.position, endPointTree.position) > 0.9f)
        {
            grabObject.transform.position =
                Vector3.SmoothDamp(grabObject.transform.position, endPointTree.position, ref velocity, .2f, speedMove);
            grabObject.transform.localScale =
                Mathf.SmoothDamp(grabObject.transform.localScale.x, 0, ref scaleVelocity, 5f) * Vector3.one;
            yield return null;
        }
        audio.Play();
        Destroy(grabObject);
        int count = 0;
        
        while (count<2)
        {
            var instantiateObject = Instantiate(firewoodPrefab, endPointTree.transform.position, Quaternion.identity);
            instantiateObject.layer = 13;
        
            while (Vector3.Distance(instantiateObject.transform.position, endPointFirewood.position) > 0.5f)
            {
                instantiateObject.transform.position =
                    Vector3.SmoothDamp(instantiateObject.transform.position, endPointFirewood.position, ref velocity, .4f, speedMove);
                yield return null;
            }

            count++;
        }
        audio.Stop();

        onStart = false;
        
        yield return null;
    }
}
