﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class LoadScore : MonoBehaviour
{
    [SerializeField] private Text scoreTime;
    [SerializeField] private Text nameText;
    [SerializeField] private GameObject inputField;
    [SerializeField] private InputField nameInputField;
    [SerializeField] private Button OkButton;
    [SerializeField] private Button reset;
    
    private string name;
    void Start()
    {
        OkButton.onClick.AddListener(OnSaveName);
        reset.onClick.AddListener(Reset);
        
        if (PlayerPrefs.GetString("Name","NoName").Equals("NoName"))
        {
            inputField.SetActive(true);
        }
        else
        {
            inputField.SetActive(false);
            name = PlayerPrefs.GetString("Name");
        }

        print($"{name}");
        nameText.text = name;
        var TimePassed = PlayerPrefs.GetFloat("Score");
        int minutes = (int) TimePassed / 60;
        int seconds = (int) TimePassed % 60;
        scoreTime.text = $"{minutes:00}:{seconds:00}";
    }

    private void OnSaveName()
    {
        name = nameInputField.text;
        nameText.text = name;
        inputField.SetActive(false);
        PlayerPrefs.SetString("Name",name);
    }

    private void Reset()
    {
        PlayerPrefs.DeleteKey("Score");
        PlayerPrefs.DeleteKey("Name");
        SceneManager.LoadScene("MainMenuScene");
    }
}
