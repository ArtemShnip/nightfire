﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MainMenu : MonoBehaviour
{
    [SerializeField] private Button playButton;
    [SerializeField] private Button exitButton;
    [SerializeField] private Button studyButton;
    [SerializeField] private Text scoreText;
    
    // Start is called before the first frame update
    void Start()
    {
        playButton.onClick.AddListener(OnPlayGame);
        exitButton.onClick.AddListener(OnExitGame);
        studyButton.onClick.AddListener(OnStudyGame);
    }

    private void OnStudyGame()
    {
        PlayerPrefs.DeleteKey("Tutorial");
        SceneManager.LoadScene("MainMenuScene");
    }

    private void OnPlayGame()
    {
        SceneManager.LoadScene("GameScene");
    }

    private void OnExitGame()
    {
        Application.Quit();
    }

    private void Score(Text score)
    {
        scoreText = score;
    }
}
