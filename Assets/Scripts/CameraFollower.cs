﻿using UnityEngine;

public class CameraFollower : MonoBehaviour
{
    public Transform Target;
    
    private Vector3 velocity;
    [SerializeField] private Vector3 posCamera = new Vector3(0,10,-5);

    private void Awake()
    {
        transform.position = Target.position + posCamera;
    }

    private void Update()
    {
        transform.position = Vector3.SmoothDamp(transform.position, Target.position+ posCamera, ref velocity, 0.18f, 20);
    }
}
