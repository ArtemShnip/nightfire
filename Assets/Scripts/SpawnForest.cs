﻿using UnityEngine;

public class SpawnForest : MonoBehaviour
{
    [SerializeField] private GameObject lightPrefab;
    [SerializeField] private GameObject mushroom;
    [SerializeField] private GameObject tree;
    [SerializeField] private GameObject penek;
    [SerializeField] private GameObject[] mushroomsPrefab;
    [SerializeField] private  GameObject[] treesPrefab;
    [SerializeField] private GameObject bush;
    [SerializeField] private GameObject bushPrefab;
    private void Start()
    {
        for (int i = 0; i < tree.transform.childCount; i++)
        {
            if (tree.transform.GetChild(i).childCount == 0)
            {
                var instantiateObject = Instantiate(penek, tree.transform.GetChild(i).position, tree.transform.GetChild(i).rotation, tree.transform.GetChild(i));
                instantiateObject.GetComponent<Rigidbody>().isKinematic = true;
                instantiateObject.AddComponent(typeof(RespawnTree));
            }
        }
        MushroomSpawn();
        TreeSpawn();
        SpawnBush();
    }

    private void MushroomSpawn()
    {
        for (int i = 0; i < mushroom.transform.childCount; i++)
        {
            var pos = mushroom.transform.GetChild(i).position;
            pos.y += 1.35f;
            var instantiateLight= Instantiate(lightPrefab, pos, Quaternion.identity, mushroom.transform.GetChild(i));
            if (mushroom.transform.GetChild(i).childCount == 1)
            {
                mushroom.transform.GetChild(i).gameObject.AddComponent(typeof(RespawnMushrooms));
                var mushroomPrefab = mushroomsPrefab[Random.Range(0,mushroomsPrefab.Length)];
                var instantiateObject = Instantiate(mushroomPrefab, mushroom.transform.GetChild(i).position, Quaternion.identity, mushroom.transform.GetChild(i));
                instantiateObject.layer = 14;
                instantiateObject.GetComponent<Rigidbody>().isKinematic = true;
            }
        }
    }
    
    private void TreeSpawn()
    {
        for (int i = 0; i < tree.transform.childCount; i++)
        {
            for (int j = 0; j < tree.transform.GetChild(i).childCount; j++)
            {
                if (tree.transform.GetChild(i).GetChild(j).childCount == 2)
                {
                    var treePrefab = treesPrefab[Random.Range(0,treesPrefab.Length)];
                    var instantiateObject = Instantiate(treePrefab, tree.transform.GetChild(i).position, tree.transform.GetChild(i).rotation, tree.transform.GetChild(i).GetChild(j));
                    instantiateObject.layer = 10;
                    instantiateObject.GetComponent<Rigidbody>().isKinematic = true;
                }
            }
        }
    }

    private void SpawnBush()
    {
        var instantiateObject = Instantiate(bushPrefab, bush.transform.position, bush.transform.rotation, bush.transform);
        instantiateObject.GetComponent<Rigidbody>().isKinematic = true;
        instantiateObject.AddComponent(typeof(Bush));
    }
}
