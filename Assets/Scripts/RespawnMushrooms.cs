﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RespawnMushrooms : MonoBehaviour
{
    [SerializeField] private GameObject[] mushroomsPrefab;
    [SerializeField] private float timeRepawn = 180f;
    
    private GameObject pointMushroom;
    private Coroutine coroutine;
    private bool OnStartRespawn;
    
    private void Start()
    {
        mushroomsPrefab = new GameObject[3];
        
        mushroomsPrefab[0] = Resources.Load<GameObject>("MushroomBlue");
        mushroomsPrefab[1] = Resources.Load<GameObject>("MushroomRed");
        mushroomsPrefab[2] = Resources.Load<GameObject>("MushroomGreen");
        
        pointMushroom = gameObject;
        
        InvokeRepeating(nameof(CheckRespawn),1f,2f);
    }
    
    private void CheckRespawn()
    {
        if (pointMushroom.gameObject.transform.childCount == 1 && !OnStartRespawn)
        {
            pointMushroom.transform.GetChild(0).gameObject.SetActive(false);
            coroutine = StartCoroutine(StartRespawn());
            coroutine = null;
        }
    }
    
    private IEnumerator StartRespawn()
    {
        OnStartRespawn = true;
        yield return new WaitForSeconds(timeRepawn);
        
        var mushroomPrefab = mushroomsPrefab[Random.Range(0,mushroomsPrefab.Length)];
        var instantiateObject = Instantiate(mushroomPrefab, pointMushroom.transform.position, Quaternion.identity, pointMushroom.transform);
        instantiateObject.layer = 14;
        instantiateObject.GetComponent<Rigidbody>().isKinematic = true;
        
        pointMushroom.transform.GetChild(0).gameObject.SetActive(true);
        OnStartRespawn = false;
        yield break;
    }
}
