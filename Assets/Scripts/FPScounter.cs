﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FPScounter : MonoBehaviour
{
    public float timer, refresh, avgFramerate;
    string display = "{0} FPS";
    private Text m_Text;
 
    private void Start()
    {
        m_Text = GetComponent<Text>();
    }

    private List<float> frames = new List<float>();
 
    private void Update()
    {
        float timelapse = Time.smoothDeltaTime;
        timer = timer <= 0 ? refresh : timer -= timelapse;
        if (timer<= 0 )
        {
            avgFramerate = (int)(1f / timelapse);
            frames.Add(avgFramerate);
        }

        if (frames.Count > 60)
        {
            frames.RemoveAt(0);
        }
 
        if(timer <= 0) avgFramerate = (int) (1f / timelapse);
        m_Text.text = string.Format(display,avgFramerate.ToString());
    }
}
